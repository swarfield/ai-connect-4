#!/usr/bin/env python3

# Written by Chris Conly based on C++
# Original code provided by Dr. Vassilis 
# Athitsos, Modified by Samuel Warfield.
# Written to be Python 3.7 compatible

import sys
from MaxConnect4Game import *

def oneMoveGame(currentGame, max_depth):
    if currentGame.pieceCount == 42:    # Is the board full already?
        print('BOARD FULL\n\nGame Over!\n')
        sys.exit(0)

    currentGame.aiPlay(max_depth) # Make a move (only random is implemented)

    print('Game state after move:')
    currentGame.printGameBoard()

    currentGame.countScore()
    print('Score: Player 1 = %d, Player 2 = %d\n' % (currentGame.player1Score, currentGame.player2Score))

    currentGame.printGameBoardToFile()

def getHumanInput(game):
    val = -1

    while(val < 1 or val > 7 ):
        try:
            val = random.randint(1,7)
            #val = int(input("Which column (1-7): "))
            if not (val < 1 or val > 7):
                if (game.isMoveValid(val-1)):
                    return val
                else: 
                    val = -1
        except ValueError:
            pass
    return val

def interactiveGame(currentGame, human_next, max_depth):

    if human_next:
        if currentGame.pieceCount >= 42:
            return
        col = getHumanInput(currentGame)

        currentGame.playPiece(col-1)
        currentGame.gameFile = "human.txt"
        currentGame.printGameBoardToFile()
    else:
        col = random.randint(1,7)
        currentGame.playPiece(col-1)
        currentGame.gameFile = "computer.txt"
        currentGame.printGameBoardToFile()

        currentGame.printGameBoard()
        col = getHumanInput(currentGame)
        currentGame.playPiece(col-1)
        currentGame.gameFile = "human.txt"
        currentGame.printGameBoardToFile()


    while(currentGame.pieceCount < 42):
        currentGame.printGameBoard()
        if currentGame.pieceCount >= 42:
            return

        # AI Turn
        currentGame.aiPlay(max_depth)
        currentGame.gameFile = "computer.txt"
        currentGame.printGameBoardToFile()

        # Human Turn
        currentGame.printGameBoard()
        currentGame.countScore()
        print('Score: Player 1 = %d, Player 2 = %d\n' % (currentGame.player1Score, currentGame.player2Score))
        if currentGame.pieceCount >= 42:
            return
        col = getHumanInput(currentGame)
        #col = random.randint(1,7)
        
        currentGame.playPiece(col-1)
        currentGame.gameFile = "human.txt"
        currentGame.printGameBoardToFile()


def main(argv):
    # Make sure we have enough command-line arguments
    if len(argv) != 5:
        print('Four command-line arguments are needed:')
        print('Usage: %s interactive [input_file] [computer-next/human-next] [depth]' % argv[0])
        print('or: %s one-move [input_file] [output_file] [depth]' % argv[0])
        sys.exit(2)

    game_mode, inFile = argv[1:3]
    max_depth = int(argv[-1])

    if not game_mode == 'interactive' and not game_mode == 'one-move':
        print('%s is an unrecognized game mode' % game_mode)
        sys.exit(2)

    currentGame = maxConnect4Game() # Create a game

    # Try to open the input file
    try:
        currentGame.gameFile = open(inFile, 'r')
    except IOError:
        sys.exit("\nError opening input file.\nCheck file name.\n")

    # Read the initial game state from the file and save in a 2D list
    file_lines = currentGame.gameFile.readlines()
    currentGame.gameBoard = [[int(char) for char in line[0:7]] for line in file_lines[0:-1]]
    currentGame.currentTurn = int(file_lines[-1][0])
    currentGame.gameFile.close()

    print('\nMaxConnect-4 game\n')
    print('Game state before move:')
    currentGame.printGameBoard()

    # Update a few game variables based on initial state and print the score
    currentGame.checkPieceCount()
    currentGame.countScore()
    print('Score: Player 1 = %d, Player 2 = %d\n' % (currentGame.player1Score, currentGame.player2Score))

    if game_mode == 'interactive':
        if not argv[3] == 'computer-next' and not argv[3] == 'human-next':
            print(f"'{argv[3]}'")
            sys.exit("Please specify if the human or computer play next.")
        if argv[3] == 'human-next':
            interactiveGame(currentGame, True, max_depth) # Be sure to pass whatever else you need from the command line
        else:
            interactiveGame(currentGame, False, max_depth)
    else: # game_mode == 'one-move'
        # Set up the output file
        outFile = argv[3]
        currentGame.gameFile = outFile
        oneMoveGame(currentGame, max_depth) # Be sure to pass any other arguments from the command line you might need.


if __name__ == '__main__':
    main(sys.argv)



