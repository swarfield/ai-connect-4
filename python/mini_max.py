#!/usr/bin/env python3

# Written by Samuel Warfield
# Written to be Python 3.7 compatible

from copy import deepcopy
from random import randint
# Minimax implimented with alpha beta pruning
global max_player_num
def mini_max(position, depth=0, alpha = float("-inf"), beta = float("inf"), maximizing_player=True):

    if depth is 0 or position.isOver():
        return eval_function(position)
    
    if maximizing_player:
        global  max_player_num
        max_player_num = position.currentTurn
        max_eval = float("-inf")
        for child in position.possible_moves():
            evaled = mini_max(child[0], depth - 1, alpha, beta, False)
            if depth == 7:
                print(f"{evaled}  ", end = "")
            if evaled > max_eval:
                max_eval = evaled
                best_move = child[1]
            alpha = max(alpha, evaled)
            if beta <= alpha:
                break

        position.playPiece(best_move)
        if depth == 7:
            print(best_move)
        return max_eval

    else: # Minimizing Player
        min_eval = float("inf")
        for child in position.possible_moves():
            evaled = mini_max(child[0], depth - 1, alpha, beta, True)
            if evaled < min_eval:
                min_eval = evaled
                best_move = child[1]
            beta = min(min_eval, evaled)
            if beta <= alpha:
                break
        
        position.playPiece(best_move)
        return min_eval

# 10*ai-10*human+num3s
def eval_function(position):
    # Update a few game variables based on initial state and print the score
    global max_player_num
    #print(max_player_num)
    #input()
    point_const = 10
    position.checkPieceCount()
    position.countScore()

    if max_player_num is 1:
        points = position.player1Score * point_const - position.player2Score * (point_const + .1)
    else:
        points = position.player2Score * point_const - position.player1Score * (point_const + .1)

     #+num 3's
    return points
    
    