Samuel Warfield 10731212

Python 3.7

I used the python example code, I then added the ai.py module that
contains the alpha beta pruned minimax implementation.

Using a terminal launch the python script as follows
./maxconnect4.py one-move <intput file> <outtput file> <max search depth>
./maxconnect4.py interactive <intput file> <human-next or computer-next> <max search depth>

Or 

python3.7 maxconnect4.py one-move <intput file> <outtput file> <max search depth>
python3.7 maxconnect4.py interactive <intput file> <human-next or computer-next> <max search depth>